package it.games;

import java.awt.Color;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class Cella2048 extends JLabel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int asseX;
	private int asseY;
	private int valore;

	private Cella2048() {
		setOpaque(true);
		setFont(new Font(null, Font.BOLD, 30));
		setHorizontalAlignment(JTextField.CENTER);
		setBackground(null);
		setBorder(BorderFactory.createLineBorder(Color.gray, 5));
		setFocusable(false);
	}

	public Cella2048(int x, int y) {
		this();
		this.asseX = x;
		this.asseY = y;
	}
	
	public void setText(int i) {
		// Assegno innanzitutto il valore intero
		setValore(i);
		if (i == 0) {
			setText(null);
			setBackground(getBackground());
		} else if (i <= 4) {
			setForeground(getForeground());
			setBackground(getBackground());
			setText(String.valueOf(i));
		} else {
			setForeground(getForeground());
			setBackground(getBackground());
			setText(String.valueOf(i));
		}
	}

	public Color getForeground() {
		return valore < 16 ? new Color(0x776e65) : new Color(0xf9f6f2);
	}

	public Color getBackground() {
		switch (valore) {
		case 2:
			return new Color(0xeee4da);
		case 4:
			return new Color(0xede0c8);
		case 8:
			return new Color(0xf2b179);
		case 16:
			return new Color(0xf59563);
		case 32:
			return new Color(0xf67c5f);
		case 64:
			return new Color(0xf65e3b);
		case 128:
			return new Color(0xedcf72);
		case 256:
			return new Color(0xedcc61);
		case 512:
			return new Color(0xedc850);
		case 1024:
			return new Color(0xedc53f);
		case 2048:
			return new Color(0xedc22e);
		}
		return new Color(0xcdc1b4);
	}

	public int getAsseX() {
		return asseX;
	}

	public void setAsseX(int asseX) {
		this.asseX = asseX;
	}

	public int getAsseY() {
		return asseY;
	}

	public void setAsseY(int asseY) {
		this.asseY = asseY;
	}

	public int getValore() {
		return valore;
	}

	public void setValore(int valore) {
		this.valore = valore;
	}

	@Override
	public String toString() {
		return "Cella2048 [asseX=" + asseX + ", asseY=" + asseY + ", valore=" + valore + "]";
	}

}
