package it.games;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class Player2048 extends JComponent {

	private static final long serialVersionUID = 1L;

	private static final int size = 4;

	private JLabel jLabelScore;
	private Board board;

	public Player2048() {
		setLayout(new BorderLayout());
		setFocusable(true);
		addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					System.out.println("RESET");
					init();
				}
				switch (e.getKeyCode()) {
				case KeyEvent.VK_LEFT:
					board.move(1, 0, false);
					break;
				case KeyEvent.VK_RIGHT:
					board.move(-1, 0, false);
					break;
				case KeyEvent.VK_DOWN:
					board.move(0, -1, false);
					break;
				case KeyEvent.VK_UP:
					board.move(0, 1, false);
					break;
				}
				//in ogni caso aggiorno il Punteggio
				jLabelScore.setText(String.valueOf(board.getScore()));
				
				/*
				 * Update alla fine controllo se ci sono celle Vuote se non ci sono controllo se
				 * è possibile fare una mossa se non è possibile hai perso!
				 */
				if (board.getEmptyCells().size() == 0 && !board.canMove()) {
					JOptionPane.showMessageDialog(null, "Hai Perso Punteggio Finale " + board.getScore());
				}
			}
		});
		init();
	}

	private void init() {
		final JPanel jPanel = build2048Frame();
		final JPanel jScore = buildScoreFrame();
		
		add(jScore, BorderLayout.NORTH);
		add(jPanel, BorderLayout.CENTER);

		board.addRandom();
		board.addRandom();
	}

	private JPanel buildScoreFrame() {
		JPanel jScore = new JPanel();
		JLabel jLabel = new JLabel("Punteggio  ");
		jLabelScore = new JLabel("0");

		jScore.setLayout(new BoxLayout(jScore, BoxLayout.X_AXIS));
		jScore.add(jLabel);
		jScore.add(jLabelScore);
		
		return jScore;
	}
	
	private JPanel build2048Frame() {
		JPanel res = new JPanel(new GridLayout(size, size));
		board = new Board(size);

		for (int y = 0; y < size; y++) {
			for (int x = 0; x < size; x++) {

				Cella2048 cell = new Cella2048(x, y);
				res.add(cell);
				// aggiungo all'oggetto di lavoro
				board.getCells().add(cell);
			}
		}
		return res;
	}

}
