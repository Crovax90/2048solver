package it.games;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class Board {

	private int score;
	private List<Cella2048> cells;

	public Board(int size) {
		this.cells = new ArrayList<Cella2048>(size * size);
	}

	public List<Cella2048> getEmptyCells() {
		return cells.stream().filter(i -> i.getValore() == 0).collect(Collectors.toList());
	}

	public void addRandom() {
		List<Cella2048> emptyCells = getEmptyCells();
		/*
		 * La regola dice che c'e'una possibilita del 90% che esca un 2 e del 10% che
		 * esca un 4 come nuovo valore
		 */
		emptyCells.get((int) (Math.random() * emptyCells.size()) % emptyCells.size())
				.setText(Math.random() < 0.9 ? 2 : 4);
	}

	boolean canMove() {
		return this.move(1, 0, true) || this.move(-1, 0, true) || this.move(0, -1, true) || this.move(0, 1, true);
	}

	boolean move(int x, int y , boolean check) {

		/**
		 * COLLECTOR su X o su Y
		 */
		Collector<Cella2048, ?, Map<Integer, List<Cella2048>>> collector = x != 0
				? Collectors.groupingBy(Cella2048::getAsseY)
				: Collectors.groupingBy(Cella2048::getAsseX);

		/*
		 * JAVA 8 COMPARATORE in una RIGA TESTATO E FUNZIONANTE
		 */
		Comparator<Cella2048> comparatore = x != 0
				? x > 0 ? Comparator.comparing(Cella2048::getAsseX)
						: Comparator.comparing(Cella2048::getAsseX).reversed()
				: y > 0 ? Comparator.comparing(Cella2048::getAsseY)
						: Comparator.comparing(Cella2048::getAsseY).reversed();

		boolean movimento = false;
		// si muove l'orizzontale in qualche direzione
		// raggruppo la lista in mappa di y -> bean
		Map<Integer, List<Cella2048>> mappa = cells.stream().collect(collector);

		if (check) {
			System.out.println("Invocato Check");
			mappa = new HashMap<Integer, List<Cella2048>>(mappa);
		}
		
		for (List<Cella2048> list : mappa.values()) {
			Collections.sort(list, comparatore);
			// Serve della ricorsione
			if (shiftCells(list))
				movimento = true;
		}
		// Aggiungo un pezzo solo se ho mosso delle celle
		if (movimento && !check)
			addRandom();

		return movimento;
	}

	private boolean shiftCells(List<Cella2048> list) {
		boolean shift = false;
		Cella2048 prev = null;
		for (Cella2048 cella : list) {

			if (prev != null && cella.getValore() != 0) {
				// Qui scorre di una posizione la cella perche quella vicino alla rotazione e'
				// libera
				if (prev.getValore() == 0) {
					shift = true;
					prev.setText(cella.getValore());
					cella.setText(0);
					// Recursion
					shiftCells(list);
				}
				// Qui li sommo perche sono uguali e cancello il precedente in modo da fare
				// spazio
				else if (cella.getValore() == prev.getValore()) {
					// il punteggio � dato dalla Somma delle 2 celle che sto unendo
					score += cella.getValore() + prev.getValore();
					System.out.println("PUNTEGGIO -> " + score);
					shift = true;
					prev.setText(prev.getValore() + cella.getValore());
					cella.setText(0);
					/*
					 * QUI NON APPLICO LA RICORSIONE PERCHE ho gia effettuato una mossa secondo la
					 * regola del sudoku dopo fatta una mossa non ne devo fare un altra sulla stessa
					 * riga
					 */
				}
			}
			// a prescindere
			prev = cella;
		}
		return shift;
	}

	public List<Cella2048> getCells() {
		return cells;
	}

	public int getScore() {
		return score;
	}

}
