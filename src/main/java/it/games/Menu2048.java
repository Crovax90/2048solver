package it.games;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.plaf.nimbus.NimbusLookAndFeel;

public class Menu2048 extends JComponent implements ActionListener {

	public final static Map<String, Class<?>> selectionMap;

	static {
		selectionMap = new HashMap<String, Class<?>>();
		selectionMap.put("Player", Player2048.class);
		selectionMap.put("AI", AI2048.class);
	}

	private JFrame frame = new JFrame("2048 Pu(GG)elli");
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final Dimension dim = new Dimension(500, 500);

	private Menu2048() {
		try {
			UIManager.setLookAndFeel(NimbusLookAndFeel.class.getName());
		} catch (Exception e) {
		}
		this.setFocusable(true);
		this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.frame.setSize(dim);
		this.frame.setPreferredSize(dim);
		this.frame.setContentPane(createSelection());
		this.frame.setResizable(false);
		this.frame.setLocationRelativeTo(null);
		this.frame.setVisible(true);
	}

	private JPanel createSelection() {

		JPanel res = new JPanel(new GridLayout());

		for (Entry<String, Class<?>> selection : selectionMap.entrySet()) {
			JButton jButton = new JButton(selection.getKey());
			jButton.addActionListener(this);
			res.add(jButton);
		}
		return res;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Class<?> clazz = selectionMap.get(e.getActionCommand());

		try {
			JComponent taskFrame = (JComponent) clazz.newInstance();

			this.frame.add(taskFrame);
			// Display the window.
			this.frame.pack();
			
		} catch (Exception ex) {
			System.out.println(ex);
		}
	}

	public static void main(String[] args) {
		new Menu2048();
	}
}
